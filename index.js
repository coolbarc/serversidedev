var rect = require('./rectangle');

function solveRect(l, b) {
    console.log("Solving for rectangle with l = " + l + " and b = " + b);
    rect(l, b, (err, rect) => {
        if (err) {
            console.log("ERROR: ", err.message);
        }
        else {
            console.log("The area of the rectangle of dimentions l = " + l + " and b = " + b + " is " + rect.area());
            console.log("The perimeter of the rectangle of dimentions l = " + l + " and b = " + b + " is " + rect.perimeter());
        }
    })
    console.log("This is called befor rectangle calculation");
}

solveRect(2, 4);
solveRect(-3, 5);
solveRect(0, 5);
solveRect(3, 8);